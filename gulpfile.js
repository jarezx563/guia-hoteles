
'use strict';

var gulp = require('gulp');
var  sass = require('gulp-sass');
     sass.compiler = require('node-sass');

const  browserSync = require('browser-sync').create();
var del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    clean = require('gulp-clean'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');


gulp.task('sass',  function(){
     return gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
 });

 // as this is gul 4.x version we must to use gulp.series to invoke other task

 gulp.task('sass:watch', function () {
    gulp.watch('./css/*scss', gulp.series('sass'));
  });

gulp.task('browser-sync', function(){
    var files= ['./*.html', './css/*.css', './images/*.{png, jpg, gif}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

/* Se requiere parallel, con parametro primero sass watch para que vea los cambios en los 
archivo sass y luego se levanta el browser-sync, se dejo con parallel porque con el método
series no funcionaba como se deseaba*/

gulp.task('default', gulp.parallel('sass:watch', 'browser-sync'), async function(){
    //gulp.start();
});




gulp.task('clean', function(){
    return gulp.src('dist', {read:false})
    .pipe(clean());   
});

gulp.task('copyfonts', function(){
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'));
 })
 

gulp.task('imagemin', function(){
    return gulp.src('./images/*.{png,jpg,jpeg,gif}')
    .pipe(imagemin({optimizationLevel: 3, progressive:true, interlaced:true}))
    .pipe(gulp.dest('dist/images'));   
});

gulp.task('usemin', function(){
    return gulp.src('./*.html')
    .pipe(flatmap(function(stream, file){
       return stream
       .pipe(usemin({
           css: [rev()],
           html: [function(){ return htmlmin({collapseWhitespace:true})}],
           js: [uglify(), rev()],
           inlinejs: [uglify()],
           inlinecss: [cleanCss(), 'concat']

       }));           
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('build', gulp.series('clean','copyfonts','imagemin', 'usemin'), async function(){
    // code here
});